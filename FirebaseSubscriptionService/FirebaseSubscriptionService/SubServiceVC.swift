//
//  ViewController.swift
//  FirebaseSubscriptionService
//
//  Created by Devin on 6/6/17.
//  Copyright © 2017 ProObject. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    // MARK: - initializations
    
    //initialize all the important boys
    var userDict : [String: Any] = [:]
    var allUsers : [String] = [] //picker data source
    var subscriptions : [String] = [] //topics that the user is subscribed to
    var chosenTopic: String = ""
    
    let defaults = UserDefaults.standard
    
    //UI Elements
    @IBOutlet weak var topicPickerView: UIPickerView!
    @IBOutlet weak var subButton: UIButton!
    @IBOutlet weak var viewSubButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.topicPickerView.dataSource = self
        self.topicPickerView.delegate = self
        
        userDefSetup()
        viewSetup()
        getJSONData()
        
        //For firebase token ---- used for testing on new device only
        //let token = Messaging.messaging().fcmToken
        //print("FCM token: \(token ?? "")")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - pickerView delegate functions
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return allUsers.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return allUsers[row]
    }
    
    //set behvior of chosen topic here:
    //If already subscribed, set button to unsubscribe
    //If not subscribed, set button to subscribe
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        chosenTopic = allUsers[row]
        if subscriptions.contains(chosenTopic) {
            subButton.setTitle("Unsubscribe", for: .normal)
        } else {
            subButton.setTitle("Subscribe", for: .normal)
        }
    }
    
    // MARK: - button listeners
    
    //subscribe or unsbuscribe from topic currently selected in chosenTopic variable
    @IBAction func subButtonListener(_ sender: Any) {
        if subscriptions.contains(chosenTopic) {
            if let index = subscriptions.index(of: chosenTopic) {
                subscriptions.remove(at: index)
            } else {
                print("Error: Element not in array") //should never be called
            }
            subButton.setTitle("Subscribe", for: .normal)
            
            //firebase unsubscribe
            Messaging.messaging().unsubscribe(fromTopic: chosenTopic)
        } else {
            subscriptions.append(chosenTopic)
            subButton.setTitle("Unsubscribe", for: .normal)
            
            //firebase subscribe
            Messaging.messaging().subscribe(toTopic: chosenTopic)
        }
        
        defaults.set(subscriptions, forKey: "SubscriptionDefaults")
        
        print("-----MARK-----")
        for e in subscriptions {
            print(e)
        }
    }
    
    
    // MARK: - segue overrides
    
    //when viewSubButton pushed, bring up a list of who the user is subscribed to. If none, then show an alert
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if subscriptions.count == 0 {
            let alert = UIAlertController(title: "No Subscriptions", message: "You currently have no subscriptions, subscribe to runners to view subscriptions.", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
                
            }))
            
            present(alert, animated: true, completion: nil)
            
            return false
        } else {
            return true
        }
    }
    
    //Pass subscriptions to next controller (SubListVC.swift)
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSubscriptions" {
            if let nextScene = segue.destination as? SubListVC {
                nextScene.subscriptions = subscriptions
            }
        }
    }
    
    // MARK: - view setup
    
    //create the button format
    func viewSetup() {
        buttonFormat(button: subButton)
        buttonFormat(button: viewSubButton)
    }
    
    func buttonFormat(button: UIButton) {
        button.backgroundColor = .lightGray
        button.setTitleColor(.black, for: .normal)
        button.layer.cornerRadius = 5
    }
    
    //set up the user defaults to save the clients subscription
    func userDefSetup() {
        subscriptions = defaults.object(forKey: "SubscriptionDefaults") as? [String] ?? [String]()
        defaults.set(subscriptions, forKey: "SubscriptionDefaults") //maybe? Yes
    }
    
    //Get data from website and put it in allUsers array. These are the topics that can be subscribed and
    //unsubscribed to.
    func getJSONData() {
        let subListURL: String = "http://208.109.53.180/runnerbuddy/user/list/"
        
        guard let url = URL(string: subListURL) else {
            print("Error: cannot create URL")
            return
        }
        
        //Snag topics from the database
        URLSession.shared.dataTask(with:url) { (data, response, error) in
            guard error == nil else {
                print(error!)
                return
            }
            
            //format data as a JSON
            let input : NSString! = (NSString(data: data!, encoding: String.Encoding.utf8.rawValue))
            var customCharSet = CharacterSet.whitespacesAndNewlines
            customCharSet.insert(charactersIn: "[]")
            let trimmedInput : NSString! = input.trimmingCharacters(in: customCharSet) as NSString
            
            let inputJSONArray = trimmedInput.components(separatedBy: ",")
            
            for json in inputJSONArray {
                self.userDict = self.convertToDictionary(text: json as String)!
                self.allUsers.append(self.userDict["user_name"] as! String)
            }
            
//            for element in self.allUsers {
//                print(element)
//            }
            
            //allUsers now contains all users from database
            
            DispatchQueue.main.async {
                self.topicPickerView.reloadAllComponents()
                self.pickerView(self.topicPickerView, didSelectRow: 0, inComponent: 0) //better solution?
                self.chosenTopic = self.allUsers[0]
            }
            
            }.resume()
        
    }
    
    //Convenience method to convert a JSON string to a dictionary
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

}

